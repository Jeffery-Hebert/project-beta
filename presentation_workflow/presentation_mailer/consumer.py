import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq")
        )
        channel = connection.channel()
        channel.queue_declare(queue="scheduled")

        def schedule(ch, method, properties, body):
            data = json.loads(body)
            email = data["email"]
            name = data["name_customer"]
            issue = data["reason"]
            send_mail(
                "Your appointment has been scheduled.",
                f"Hello {name}, your appoint for {issue} has been scheduled. We look forward to assisting you.",
                "admin@carcar.com",
                [email],
                fail_silently=False,
            )

        channel.basic_consume(
            queue="scheduled",
            on_message_callback=schedule,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
